# To do list
> Django To do list

## 설치 방법

```sh
pip3 install -r requirements.txt
```

```sh
python manage.py makemigrations
python manage.py migrate
```

## 사용 예제

```sh
python manage.py runserver
```

![](https://gitlab.com/yonghyunlee/Django_todolist/raw/master/resources/images/%EC%8A%A4%ED%81%AC%EB%A6%B0%EC%83%B7%202019-02-19%20%EC%98%A4%ED%9B%84%202.12.39.png)

![](https://gitlab.com/yonghyunlee/Django_todolist/raw/master/resources/images/%EC%8A%A4%ED%81%AC%EB%A6%B0%EC%83%B7%202019-02-19%20%EC%98%A4%ED%9B%84%202.12.48.png)
