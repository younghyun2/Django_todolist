# Generated by Django 2.0.7 on 2018-08-25 11:25

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('todolist', '0002_auto_20180809_2200'),
    ]

    operations = [
        migrations.AddField(
            model_name='todolist',
            name='owner',
            field=models.ForeignKey(null=True, on_delete=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
