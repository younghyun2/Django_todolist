from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy


from todolist.forms import PostSearchForm, CreateForm, UpdateForm
from todolist.models import ToDoList
from django.views.generic import ListView, DetailView, FormView, CreateView, UpdateView, DeleteView
from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin


class ToDoListLV(LoginRequiredMixin, ListView):
    login_url = '/accounts/login/'
    model = ToDoList

    def get_queryset(self):
        queryset = super(ToDoListLV, self).get_queryset()
        return queryset.filter(end_date__gt=datetime.now()).filter(owner=self.request.user)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['today_work'] = super(ToDoListLV, self).get_queryset().filter(end_date__year=datetime.now().year,
                                end_date__month=datetime.now().month,
                                end_date__day=datetime.now().day).filter(owner=self.request.user)
        context['finish_work'] = super(ToDoListLV, self).get_queryset().filter(end_date__lt=datetime.now()).filter(
            owner=self.request.user)

        return context


class ToDoListCal(ListView):
    template_name = 'todolist/todolist_calendar.html'
    model = ToDoList

    def get_queryset(self):
        queryset = super(ToDoListCal, self).get_queryset()
        return queryset.filter(owner=self.request.user)


class Post(object):
    pass


class SearchFormView(FormView):
    form_class = PostSearchForm
    template_name = 'todolist/post_search.html'

    def form_vaild(self, form):
        schWord = '%s' % self.request.POST['search_word']
        post_list = Post.objects.filter(
            Q(title__icontains=schWord) | Q(description__icontains=schWord) | Q(content__icontains=schWord)).distinct()

        context = {}
        context['form'] = form
        context['search_term'] = schWord
        context['objects_list'] = post_list

        return render(self.request, self.template_name, context)


class ToDolistCreate(LoginRequiredMixin, CreateView):
    form_class = CreateForm
    model = ToDoList
    # fields = ['title', 'content', 'start_date', 'end_date']

    success_url = reverse_lazy('todolist:index')

    def form_valid(self, form):  # 3
        form.instance.owner = self.request.user  # 4
        return super(ToDolistCreate, self).form_valid(form)  # 5


class ToDolistUpdate(LoginRequiredMixin,UpdateView):
    template_name = "todolist/todolist_update.html"
    form_class = UpdateForm
    model = ToDoList
    # fields = ['title', 'content', 'start_date', 'end_date']
    success_url = reverse_lazy('todolist:index')

    def form_valid(self, form):  # 3
        form.instance.owner = self.request.user  # 4
        return super(ToDolistUpdate, self).form_valid(form)  #



class ToDolistDelete(LoginRequiredMixin, DeleteView):
    model = ToDoList
    success_url = reverse_lazy('todolist:index')
