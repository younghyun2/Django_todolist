from django.contrib import admin
from .models import ToDoList

# Register your models here.

class TodolistAdmin(admin.ModelAdmin):
    list_display = ('title', 'start_date', 'end_date')
    search_fields = ('title', 'content')

admin.site.register(ToDoList, TodolistAdmin)