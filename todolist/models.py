from django.db import models
from datetime import datetime

from django.contrib.auth.models import User
# Create your models here.


class ToDoList(models.Model):
    title = models.CharField('TITLE', max_length=50)
    content = models.TextField('CONTENT', max_length=100, blank=True, help_text='simple content text.')
    start_date = models.DateTimeField('Start Date')
    end_date = models.DateTimeField('End Date')
    owner = models.ForeignKey(User, null=True, on_delete=True)

    def __str__(self):
        return self.title

